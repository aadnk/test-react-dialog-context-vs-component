# React Dialog: Context vs. Component

This project serves as a demonstration of a pitfall when using React's Context API to manage the props and state of a dialog component. Two approaches are presented: one using the Context API (`buggyMainPage.tsx`) and another using a regular React component with props (`fixedMainPage.tsx`).

#### Problem Statement:

The application shows a dialog when a button is clicked. This dialog contains a text field for the user to enter their name. When the user clicks the "Okay" button in the dialog, an alert should pop up greeting the user by the name they entered. 

#### The Buggy Version (`buggyMainPage.tsx`):

In this approach, a context provider (`DialogProvider`) is used to manage the state of the dialog (whether it's showing and its options). When the user interacts with the dialog, the state updates don't reflect immediately in the context's consumers, likely due to an complex interaction between hooks and closures.

#### The Fixed Version (`fixedMainPage.tsx`):

By converting the dialog to a regular component that accepts props, the state management becomes more direct and predictable. The dialog's visibility and options are managed through local state and props, making it reactive to user interactions as expected.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
