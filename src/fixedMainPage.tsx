import React, { useState } from 'react';
import logo from './logo.svg';
import { TextField } from '@mui/material';
import CustomDialog from './dialogs/custom-dialog';

function MainPage() {
    const [dialogShowing, setDialogShowing] = useState(false);
    const [name, setName] = useState('');

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                
                <button onClick={() => setDialogShowing(true)}>Show Dialog</button>
                
                <CustomDialog
                    title="Test Dialog"
                    positiveButtonText="Okay"
                    negativeButtonText="Cancel"
                    positiveButtonOnClick={() => {
                        alert(`Hello ${name}!`);
                        setDialogShowing(false);
                    }}
                    negativeButtonOnClick={() => {
                        console.log('Cancel clicked!');
                        setDialogShowing(false);
                    }}
                    isOpen={dialogShowing}
                >
                    <TextField helperText={"Enter your name"} onChange={(e) => setName(e.target.value)}></TextField>
                </CustomDialog>
            </header>
        </div>
    );
}

export default MainPage;
