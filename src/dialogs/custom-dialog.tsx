import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import React from 'react';

interface DialogProps {
    title: string;
    positiveButtonText: string;
    negativeButtonText: string;
    positiveButtonOnClick: () => void;
    negativeButtonOnClick: () => void;
    isOpen: boolean;
    onClose?: () => void;
    children?: React.ReactNode;
}

const CustomDialog: React.FC<DialogProps> = ({
    title,
    positiveButtonText,
    negativeButtonText,
    positiveButtonOnClick,
    negativeButtonOnClick,
    isOpen,
    onClose,
    children
}) => {
    return (
        <Dialog open={isOpen} onClose={onClose}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                {children}
            </DialogContent>
            <DialogActions>
                <Button onClick={positiveButtonOnClick} color="primary">
                    {positiveButtonText}
                </Button>
                <Button onClick={negativeButtonOnClick} color="primary">
                    {negativeButtonText}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default CustomDialog;
