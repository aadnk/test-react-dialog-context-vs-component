import React, { createContext } from 'react';

export interface DialogState {
    title: string;
    positiveButtonText: string;
    negativeButtonText: string;
    positiveButtonOnClick: () => void;
    negativeButtonOnClick: () => void;
    children: React.ReactNode;
}

export interface DialogContextType {
    dialogShowing: boolean;
    setDialogShowing: React.Dispatch<React.SetStateAction<boolean>>;
    dialogOptions: DialogState;
    setDialogOptions: React.Dispatch<React.SetStateAction<DialogState>>;
}

export const DialogContext = createContext<DialogContextType>({
    dialogShowing: false,
    setDialogShowing: () => {},
    dialogOptions: {
        title: '',
        positiveButtonText: '',
        negativeButtonText: '',
        positiveButtonOnClick: () => {},
        negativeButtonOnClick: () => {},
        children: null
    },
    setDialogOptions: () => {},
});
