import { useState } from "react";
import { DialogState, DialogContext } from "./dialog-context";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from "@mui/material";

export const DialogProvider = ({ children }: { children: React.ReactNode }) => {
    const [dialogShowing, setDialogShowing] = useState(false);
    const [dialogOptions, setDialogOptions] = useState<DialogState>({
        title: '',
        positiveButtonText: '',
        negativeButtonText: '',
        positiveButtonOnClick: () => {},
        negativeButtonOnClick: () => {},
        children: null
    });

    return (
        <DialogContext.Provider value={{ dialogShowing, setDialogShowing, dialogOptions, setDialogOptions }}>
            {children}
            <Dialog open={dialogShowing} onClose={() => setDialogShowing(false)}>
                <DialogTitle>{dialogOptions.title}</DialogTitle>
                <DialogContent>
                    {dialogOptions.children}
                </DialogContent>
                <DialogActions>
                    <Button onClick={dialogOptions.positiveButtonOnClick} color="primary">
                        {dialogOptions.positiveButtonText}
                    </Button>
                    <Button onClick={dialogOptions.negativeButtonOnClick} color="primary">
                        {dialogOptions.negativeButtonText}
                    </Button>
                </DialogActions>
            </Dialog>
        </DialogContext.Provider>
    );
};
