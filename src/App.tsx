import React, { useContext } from 'react';
import './App.css';
import { DialogProvider } from './dialogs/dialog-provider';
import BuggyMainPage from './buggyMainPage';
import MainPage from './fixedMainPage';

function App() {
  let testBuggy = false;

  if (testBuggy) {
    // This is the buggy version
    return (
      <DialogProvider>
        <BuggyMainPage></BuggyMainPage>
      </DialogProvider>
    );
  } else {
    // This is the fixed version
    return (
      <MainPage></MainPage>
    );
  }
}

export default App;
