import { useContext, useState } from 'react';
import logo from './logo.svg';
import { DialogContext } from './dialogs/dialog-context';
import { TextField } from '@mui/material';

function BuggyMainPage() {
    /**
     * Issue:
     *  When I open the Web App, and click "Show Dialog", I get presented with a dialog containing a TextField 
     *  saying "Enter your name".  So far it seems to work.
     *
     *  Then I enter a name, say John, and click "Okay". I then get an alert containing the text "Hello !". 
     *
     *  If I open the dialog again, and write a new name, say Mary and click "Okay", 
     *  I now get the alert saying "Hello John!".
     */
    const { setDialogShowing, setDialogOptions } = useContext(DialogContext);

    const [ name, setName ] = useState('');

    const showDialog = () => {
        setDialogOptions({
            title: 'Test Dialog',
            positiveButtonText: 'Okay',
            negativeButtonText: 'Cancel',
            positiveButtonOnClick: () => {
                alert(`Hello ${name}!`);
                setDialogShowing(false);
            },
            negativeButtonOnClick: () => {
                console.log('Cancel clicked!');
                setDialogShowing(false);
            },
            children: <TextField helperText={"Enter your name"} onChange={(e) => setName(e.target.value)}></TextField>
        });
        setDialogShowing(true);
    };

    return (
        <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          
          <button onClick={showDialog}>Show Dialog</button>
        </header>
      </div>
    );
}

export default BuggyMainPage;